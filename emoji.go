// emoji package has some emojis that work in my terminal
package emoji

const (
	Check = "☑️"
	X     = "✖️"
	Warn  = "⚠️"
	Info  = ">"
)
